PHONY_TARGETS=help check fetch all clean veryclean build dir bootstrap pre-build setup-debian setup-fedora docker-build-image docker-run-build-job docker-remove-image
.PHONY : $(PHONY_TARGETS)

version:=$(shell cat version)
release:=$(shell cat release)
android_release:=$(shell cat android_release)
full_version:=$(version)-$(release)$(shell [ $(android_release) -gt 1 ] && echo "-$(android_release)")
ifeq ($(arch),)
arch:=x86_64
endif

mozbuild=~/.mozbuild
build_image=lw-android-arm



help :

	@echo "use: make [help] [all] [clean] [build] [package] [artifacts]"
	@echo ""
	@echo "  check     - Get the latest versioning files. MUST be done FIRST."
	@echo "  fetch     - Download the tarball."
	@echo ""
	@echo "  all       - Build librewolf and package it."
	@echo "  build     - Perform './mach build' on the extracted tarball."
	@echo "  package   - multilocale package."
	@echo ""
	@echo "  dir       - just extract and patch the LW tarball."
	@echo "  clean     - Remove output files and temporary files."
	@echo "  veryclean - Remove everything not under version control."
	@echo ""
	@echo "  bootstrap - try to set up the build environment."
	@echo "  setup-debian, setup-fedora - needed packages."
	@echo ""
	@echo "  docker-build-image   - Run 'docker build' for" $(build_image) "image."
	@echo "  docker-run-build-job - Build LW using 'docker run' on" $(build_image) "image."
	@echo "  docker-remove-image  - Remove" $(build_image) "docker image."
	@echo ""



all : build package


#
# check, fetch - update versioning files to the latest version and get the tarball.
#

check : update-version
update-version :
	@wget -qO version "https://gitlab.com/librewolf-community/browser/source/-/raw/main/version"
	@wget -qO release "https://gitlab.com/librewolf-community/browser/source/-/raw/main/release"
	@echo "Now using LibreWolf version $$(cat version)-$$(cat release)."



fetch : fetch-tarball
fetch-tarball :
	@wget -qO "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sha256sum" "https://gitlab.com/librewolf-community/browser/source/-/jobs/artifacts/main/raw/librewolf-$$(cat version)-$$(cat release).source.tar.gz.sha256sum?job=Build"
	@wget -qO "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sig" "https://gitlab.com/librewolf-community/browser/source/-/jobs/artifacts/main/raw/librewolf-$$(cat version)-$$(cat release).source.tar.gz.sig?job=Build"
	@wget --progress=bar:force -O "librewolf-$$(cat version)-$$(cat release).source.tar.gz" "https://gitlab.com/librewolf-community/browser/source/-/jobs/artifacts/main/raw/librewolf-$$(cat version)-$$(cat release).source.tar.gz?job=Build"
	cat "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sha256sum"
	sha256sum -c "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sha256sum"
	gpg --import assets/librewolf.asc
	gpg --verify "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sig" "librewolf-$$(cat version)-$$(cat release).source.tar.gz"



#
# clean, veryclean - zap the created librewolf dir (clean), or everything not under version control (veryclean)
#


clean :
	rm -rf librewolf-$(full_version)

veryclean : clean
	rm -rf firefox-$(full_version).en-US.win64.zip librewolf-$(full_version).en-US.win64-setup.exe librewolf-$(full_version).en-US.win64-portable.zip
	rm -f "librewolf-$$(cat version)-$$(cat release).source.tar.gz" "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sha256sum" "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sig"
	rm -f version release

#
# build, package - these are the main build steps.
#

build : dir
	(cd librewolf-$(full_version) && ./mach build)

package : dir
	(cd librewolf-$(full_version) && ./mach package)
#	( cd librewolf-$(full_version) && echo 'Packaging... (output hidden)' && \
#	  cat browser/locales/shipped-locales | xargs ./mach package-multi-locale --locales >/dev/null )



dir : librewolf-$(full_version) pre-build

librewolf-$(full_version) : librewolf-$(full_version).source.tar.gz
	rm -rf $@
	tar xf $<

pre-build:
	cp -v assets/mozconfig librewolf-$(full_version)
#	(cd librewolf-$(full_version) && patch -p1 -i ../assets/tryfix-reslink-fail.patch)
#	(cd librewolf-$(full_version) && patch -p1 -i ../assets/fix-l10n-package-cmd.patch)



#
# bootstrap, setup-debian, setup-fedora - getting the compile environment ready.
#


bootstrap : dir
	sh -c "assets/setup-rust.sh"
	$$HOME/.cargo/bin/rustup target add thumbv7neon-linux-androideabi
	(cd librewolf-$(full_version) && ./mach --no-interactive bootstrap --application-choice=mobile_android)

setup-debian :
	apt-get -y install mercurial python3 python3-dev python3-pip curl wget dpkg-sig  libssl-dev zstd libxml2-dev

setup-fedora :
	dnf -y install python3 curl wget zstd python3-devel python3-pip mercurial openssl-devel libxml2-devel



#
# docker-build, docker-run, docker-clean - perform the build in Docker
#


docker-build-image :
	docker build -t $(build_image) - < assets/Dockerfile

docker-run-build-job :
	docker run --rm $(build_image) sh -c "git pull && make check && make fetch && make all"

docker-remove-image :
	docker rmi $(build_image)
