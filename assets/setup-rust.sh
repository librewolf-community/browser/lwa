#!/bin/sh

set -e
tmpfile=/tmp/tmp.784979489.sh

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > $tmpfile
chmod +x $tmpfile
/bin/sh -c "$tmpfile -y"
rm -f $tmpfile
